# Test Celerates

## Getting started

This application built using Laravel 11, Mysql, Bootstrap 4

### System Requirement

- PHP 8.3
- MySQL 5.7 or 8.0 with charset general ci
- Nodejs (for mix assets if needle)

### How to Run (development)

- copyas .env.example

```bash
cp .env.example .env
```

- Filled DB configuration. Main DB_NAME is new database for this project
- Please set up ENV VARIABLE

```
APP_NAME=
APP_ENV=
APP_KEY=
APP_DEBUG=
APP_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=rmu
DB_USERNAME=root
DB_PASSWORD=

MAIL_MAILER=
MAIL_HOST=
MAIL_PORT=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=
```
- Install packages

```bash
composer install (dont update!!)
```

- Run command migration

```bash
php artisan migrate 
```

- Run clear cache

```bash
php artisan cache:clear
```

- Run command seeder

```bash
php artisan db:seed
```
- Run server

```bash
php artisan serve
```