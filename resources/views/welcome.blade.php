<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Code</title>

        <!-- Fonts -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/test.css') }}">
    </head>
    <body>
        <section id="tabs" class="project-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Data</h3>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table class="table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>id_transaksi</th>
                                            <th>kode_wilayah_kerja</th>
                                            <th>kode_produk</th>
                                            <th>kode_bank</th>
                                            <th>lob</th>
                                            <th>kode_agen</th>
                                            <th>jenis_transaksi</th>
                                            <th>id_dd_reas</th>
                                            <th>nominal</th>
                                            <th>created_date</th>
                                            <th>created_by</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($datas as $data)
                                            <tr>
                                                <td>{{ $data->id_transaksi }}</td>
                                                <td>{{ $data->kode_wilayah_kerja }}</td>
                                                <td>{{ $data->kode_produk }}</td>
                                                <td>{{ $data->kode_bank }}</td>
                                                <td>{{ $data->lob }}</td>
                                                <td>{{ $data->kode_agen }}</td>
                                                <td>{{ $data->jenis_transaksi }}</td>
                                                <td>{{ $data->id_dd_reas }}</td>
                                                <td>{{ format_uang($data->nominal) }}</td>
                                                <td>{{ $data->created_date }}</td>
                                                <td>{{ $data->created_by }}</td>
                                            </tr>
                                        @empty
                                            <div class="alert alert-danger">
                                                Data  belum Tersedia.
                                            </div>
                                        @endforelse
                                    </tbody>
                                </table>
                                {{ $datas->links() }}
                            </div>
                          
                        </div>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-md-12">
                        <h3>Jurnal</h3>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table class="table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nama Wilayah Kerja</th>
                                            <th>Kode COA (7 Segmen)</th>
                                            <th>Nominal Debet</th>
                                            <th>Nominal Kredit</th>
                                            <th>Tanggal Jurnal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $countId = [];
                                        @endphp
                                        @forelse ($datas as $k => $data)
                                            <tr>
                                                <td>{{ $data->kode_wilayah_kerja == 3 ? "Bandung" : "Kantor Pusat" }}</td>
                                                <td>{{ setCoa(  $data->kode_wilayah_kerja,
                                                                $data->jenis_transaksi,
                                                                "debit",
                                                                $data->kode_bank,
                                                                $data->lob,
                                                                $data->kode_produk,
                                                                $data->kode_agen
                                                            ) 
                                                }}</td>
                                                <td>{{ format_uang($data->nominal) }}</td>
                                                <td></td>
                                                <td>{{ getDayLast() }}</td>
                                            </tr>
                                            <tr>
                                                <td>{{ $data->kode_wilayah_kerja == 3 ? "Bandung" : "Kantor Pusat" }}</td>
                                                <td>{{ setCoa(  $data->kode_wilayah_kerja,
                                                                $data->jenis_transaksi,
                                                                "kredit",
                                                                $data->kode_bank,
                                                                $data->lob,
                                                                $data->kode_produk,
                                                                $data->kode_agen
                                                            ) 
                                                }}</td>
                                                <td></td>
                                                <td>{{ format_uang($data->nominal) }}</td>
                                                <td>{{ getDayLast() }}</td>
                                            </tr>
                                        @empty
                                            <div class="alert alert-danger">
                                                Data  belum Tersedia.
                                            </div>
                                        @endforelse
                                    </tbody>
                                </table>
                                {{ $datas->links() }}
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </body>
</html>
