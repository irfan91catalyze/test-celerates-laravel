<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datas; 
use Illuminate\View\View;
use Helper;

class DataController extends Controller
{
    
    public function index() : View
    { 
        $datas = Datas::latest()->paginate(10);
        return view('welcome', compact('datas'));
    }
}
