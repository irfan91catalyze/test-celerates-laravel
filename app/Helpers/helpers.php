<?php
   
use Carbon\Carbon;
  
/**
 * Write code on Method
 *
 * @return response()
 */
if (! function_exists('convertYmdToMdy')) {
    function convertYmdToMdy($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('m-d-Y');
    }
}
  
/**
 * Write code on Method
 *
 * @return response()
 */
if (! function_exists('convertMdyToYmd')) {
    function convertMdyToYmd($date)
    {
        return Carbon::createFromFormat('m-d-Y', $date)->format('Y-m-d');
    }
}

if (! function_exists('format_uang')) {
    function format_uang($angka){
        $hasil="Rp ".number_format($angka,0,',','.');
        return $hasil;
   }
}

function terbilang($angka) {
    $angka=abs($angka);
    $baca =array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  
    $terbilang="";
     if ($angka < 12){
         $terbilang= " " . $baca[$angka];
     }
     else if ($angka < 20){
         $terbilang= terbilang($angka - 10) . " belas";
     }
     else if ($angka < 100){
         $terbilang= terbilang($angka / 10) . " puluh" . terbilang($angka % 10);
     }
     else if ($angka < 200){
         $terbilang= " seratus" . terbilang($angka - 100);
     }
     else if ($angka < 1000){
         $terbilang= terbilang($angka / 100) . " ratus" . terbilang($angka % 100);
     }
     else if ($angka < 2000){
         $terbilang= " seribu" . terbilang($angka - 1000);
     }
     else if ($angka < 1000000){
         $terbilang= terbilang($angka / 1000) . " ribu" . terbilang($angka % 1000);
     }
     else if ($angka < 1000000000){
        $terbilang= terbilang($angka / 1000000) . " juta" . terbilang($angka % 1000000);
     }
        return $terbilang;
 }

 if (! function_exists('getDayLast')) {
    function getDayLast() {
        $date = new DateTime('now');
        $date->modify('last day of this month');
        return $date->format('Y-m-d');
    }
 }

 if (! function_exists('setCoa')) {
    function setCoa(
            $kodeWilayah,
            $jenisTransaksi, 
            $statusTransaksi,
            $kodeBank,
            $kodeLOB,
            $kodeProduk,
            $kodeAgen,
            $costCenter = 001,
        ) {
            $segment1 = "0001".$kodeWilayah.".";
            $segment2 = $kodeWilayah.".";
            $segment3 = $costCenter.".";
            $segment4 = getNaturalAccount($jenisTransaksi,$statusTransaksi);
            $segment5 = $kodeBank.".";
            $segment6 = $kodeLOB.$kodeProduk.".";
            $segment7 = ($kodeAgen <> 0) ? $kodeAgen : "000";

        return $segment1.$segment2.$segment3.$segment4.$segment5.$segment6.$segment7;
    }
 }

 function getNaturalAccount($jenisTransaksi,$statusTransaksi){
    if ($jenisTransaksi == 1 && $statusTransaksi == "debit") {
        return "12345ABC01.";
    } elseif ($jenisTransaksi == 1 && $statusTransaksi == "kredit") {
        return "12345CBA05.";
    } elseif ($jenisTransaksi == 2 && $statusTransaksi == "debit") {
        return "54321DCBA01.";
    } elseif ($jenisTransaksi == 2 && $statusTransaksi == "kredit") {
        return "54321ABCD05.";
    }
 }