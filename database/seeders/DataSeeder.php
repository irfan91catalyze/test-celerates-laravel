<?php

namespace Database\Seeders;

use App\Models\Datas;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Datas::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        collect([
            [
                'id_transaksi'=> 150130,
                'kode_wilayah_kerja' => 3,
                'kode_produk'=> 30,
                'kode_bank' => 567,  
                'lob'=> 3,
                'kode_agen' => 1,
                'jenis_transaksi'=> 1,
                'id_dd_reas' => 2,
                'nominal'=> 12500000,
                'created_date' => '2024-05-02',
                'created_by' => 661,
            ],
            [
                'id_transaksi'=> 150131,
                'kode_wilayah_kerja' => 1,
                'kode_produk'=> 30,
                'kode_bank' => 567,  
                'lob'=> 3,
                'kode_agen' => 0,
                'jenis_transaksi'=> 2,
                'id_dd_reas' => NULL,
                'nominal'=> 5000000,
                'created_date' => '2024-05-02',
                'created_by' => 661,
            ],
        ])->each(fn ($data) => Datas::create($data));
    }
}
