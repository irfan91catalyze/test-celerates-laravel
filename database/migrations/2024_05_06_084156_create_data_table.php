<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('datas', function (Blueprint $table) {
            $table->integer('id_transaksi')->unique();
            $table->integer('kode_wilayah_kerja');
            $table->integer('kode_produk');
            $table->integer('kode_bank');
            $table->integer('lob');
            $table->integer('kode_agen')->default(0);
            $table->enum('jenis_transaksi', [1,2]);
            $table->integer('id_dd_reas')->nullable();
            $table->double('nominal');
            $table->date('created_date');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data');
    }
};
